# argus-onboarding-core

## Description

This project defines the
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
"Enterprise Business Rules" layer (aka "Core") for the Argus coordinator feature. It coordinates the interactions
between video and shelf objects. It is responsible for defining the interfaces, classes and top level artifacts
supporting the coordinator feature.

The sole responsibility for the onboarding feature is to support introducing the Argus applications to new Users.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Started, see Versions.md for the current version.

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the onboarding feature.

## Test Cases

### Overview

The table below identifies the core layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions). The package is relative to `com.pajato.argus.coordinator`

| Filename Prefix  | Test Case Name                                              |
|------------------|-------------------------------------------------------------|
| OnboardingError | When creating an onboarding error instance, verify the message |
| Onboarding      | When an onboarding object is created, verify the values |
|                  | When serializing an onboarding object, verify the result |

### Notes

For specific information see the
[core layer documentation](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CoreLayer.md).

The single responsibility for this project is to provide the interface definitions used by outer architectural layers for
the Onboarding feature.

The uc (Use Case) layer uses these interfaces to specify the business logic.

The adapter layer implements these core interfaces.
