package com.pajato.argus.onboarding.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class OnboardingErrorUnitTest : ReportingTestProfiler() {

    @Test fun `When creating an onboarding error instance, verify the message`() {
        val expected = "xyzzy"
        val exc = OnboardingError(expected)
        assertEquals(expected, exc.message)
    }
}
