package com.pajato.argus.onboarding.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class OnboardingUnitTest : ReportingTestProfiler() {
    private val hasOnboarded = true
    private val tmdbApiKey = "xyzzy"
    private val networksJson = """[{"id":23},{"id":6}]"""
    private val profilesJson = """[{"id":100},{"id":101},{"id":102}]"""
    private val onboarding = Onboarding(hasOnboarded, tmdbApiKey, networksJson, profilesJson)
    private val json = Json.encodeToString(onboarding)
    private val expected = """{"hasOnboarded":true,"tmdbApiKey":"xyzzy",""" +
        """"networkListAsJson":"[{\"id\":23},{\"id\":6}]",""" +
        """"profileListAsJson":"[{\"id\":100},{\"id\":101},{\"id\":102}]"}"""

    @Test fun `When an onboarding object is created, verify the values`() {
        assertEquals(hasOnboarded, onboarding.hasOnboarded)
        assertEquals(tmdbApiKey, onboarding.tmdbApiKey)
        assertEquals(networksJson, onboarding.networkListAsJson)
        assertEquals(profilesJson, onboarding.profileListAsJson)
        assertEquals(expected, json)
    }

    @Test fun `When serializing an onboarding object, verify the result`() {
        assertEquals(json, Json.encodeToString(onboarding))
        assertEquals(onboarding, Json.decodeFromString(json))
    }
}
