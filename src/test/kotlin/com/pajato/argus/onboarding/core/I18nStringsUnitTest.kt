package com.pajato.argus.onboarding.core

import com.pajato.i18n.strings.StringsResource
import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class I18nStringsUnitTest : ReportingTestProfiler() {
    @BeforeTest fun setUp() {
        StringsResource.cache.clear()
        I18nStrings.registerStrings()
    }

    @Test fun `When strings have been registered, verify the cache size`() {
        val expected = I18nStrings.count
        assertEquals(expected, StringsResource.cache.size)
    }
}
