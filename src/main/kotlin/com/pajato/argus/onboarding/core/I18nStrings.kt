package com.pajato.argus.onboarding.core

import com.pajato.i18n.strings.StringsResource.put

object I18nStrings {
    const val ONBOARDING_UNREGISTERED_ID = "OnboardingUnregisteredId"
    const val ONBOARDING_INJECTION_ERROR = "OnboardingInjectionError"
    const val ONBOARDING_EXISTS_ERROR = "OnboardingExistsError"
    const val ONBOARDING_EMPTY_ERROR = "OnboardingEmptyError"
    const val ONBOARDING_REPO_ERROR = "OnboardingRepoError"
    const val ONBOARDING_NOT_A_FILE = "OnboardingNotAFile"
    const val ONBOARDING_DIR_ERROR = "OnboardingDirError"
    const val ONBOARDING_NOT_FOUND = "OnboardingNotFound"
    const val ONBOARDING_NO_MASTER_SHELF = "OnboardingNoMasterShelf"

    internal var count = 0

    fun registerStrings() {
        count = 0
        register(ONBOARDING_UNREGISTERED_ID, "The video shelf with id: '{{id}}' is not been registered!")
        register(ONBOARDING_INJECTION_ERROR, "Dependencies not injected")
        register(ONBOARDING_EXISTS_ERROR, "The file with path: '{{path}}' does not exist!")
        register(ONBOARDING_EMPTY_ERROR, "The file `shelf.txt' must not be empty!")
        register(ONBOARDING_REPO_ERROR, "The file with path '{{path}}' is not a valid repo path!")
        register(ONBOARDING_NOT_A_FILE, "The given file with path '{{path}}' is not a valid file!")
        register(ONBOARDING_DIR_ERROR, "The given directory '{{dir}}' is not a directory!")
        register(ONBOARDING_NOT_FOUND, "The given id '{{id}}' is not cached!. Might be an injection issue.")
        register(ONBOARDING_NO_MASTER_SHELF, "The master shelf resource could not be loaded!")
    }

    private fun register(key: String, value: String) {
        put(key, value)
        count++
    }
}
