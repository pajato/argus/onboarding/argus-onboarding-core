package com.pajato.argus.onboarding.core

import java.net.URI

interface OnboardingRepo {
    var onboarding: Onboarding?
    suspend fun persist()
    suspend fun inject(uri: URI?)
}
