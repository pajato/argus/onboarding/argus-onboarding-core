package com.pajato.argus.onboarding.core

class OnboardingError(message: String) : Exception(message)
