package com.pajato.argus.onboarding.core

import kotlinx.serialization.Serializable

/**
 * Represents the onboarding data of a user.
 *
 * @param hasOnboarded Indicates if the user has completed the onboarding process.
 * @param tmdbApiKey The API key for accessing the TMDb (The Movie Database) API.
 * @param networkListAsJson The JSON string representing the user's network list.
 * @param profileListAsJson The JSON string representing the user's profile list.
 */
@Serializable data class Onboarding(
    val hasOnboarded: Boolean = false,
    val tmdbApiKey: String = "",
    val networkListAsJson: String = "",
    val profileListAsJson: String = ""
)
